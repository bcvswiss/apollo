# apollo_and_northbound_mosquitto

This folder contains a `docker-compose.yml` file that additionally starts a mosquitto mqtt broker (with annonymus access). Can be handy for first tests. 

The northbound broker listens on port 1884