# Table of Contents 

- [apollo](#apollo)
- [Prerequisites](#prerequisites)
- [Getting started](#getting-started)
  - [Start apollo container](#start-apollo-container)
  - [Command Connector-API](#command-connector-api)
  - [Using Swagger](#using-swagger)
  - [Getting Started with Postman](#getting-started-with-postman)
    - [Device provisioning](#device-provisioning)
    - [Run io-key simulator](#run-io-key-simulator)
      - [Option 1: Standalone Windows application](#option-1-standalone-windows-application)
      - [Option 2: Docker](#option-2-docker)
    - [Read Sensor Details](#read-sensor-details)
    - [(Un-)Subscribe datasource](#un-subscribe-datasource)
- [Add MQTT TLS Support for io-key connections](#add-mqtt-tls-support-for-io-key-connections)
- [Northbound MQTT TLS](#northbound-mqtt-tls)
- [Advanced Configuration](#advanced-configuration)
  - [Northbound MQTT Topics](#northbound-mqtt-topics)
- [Known Issues](#known-issues)
# apollo 

apollo is a docker container that connects io-keys to your MQTT broker.  
apollo also provides a REST-API to

- provision io-keys
- get information about the sensors connected to the io-keys
- control the data-flow (subscribe or unsubscribe data points)
- send commands to io-keys (e.g. restart)
- configure io-keys' edge alarms

<img src="img/apollo_overview.jpg"  width="600">

# Prerequisites
- install docker 
  - [e.g. Docker Desktop for testing](https://www.docker.com/get-started)
- (optional) install dotnetcore to run the io-key simulator
  - [Microsoft download page](https://dotnet.microsoft.com/download/dotnet/3.1)
  - [windows x64 installer](https://dotnet.microsoft.com/download/dotnet/thank-you/runtime-desktop-3.1.13-windows-x64-installer)  

# Getting started
We recommend getting started with apollo in three steps. 

| 1. deploy apollo locally <br> ingest simulator data to your platform | 2. deploy apollo to your cloud <br> ingest simulator data to your platform |  3. connect an io-key to your apollo | 
| ------------ | ------------- |  ------------- | 
| <img src="img/deploy_1.jpg"  height="350">  | <img src="img/deploy_2.jpg"  height="350"> |<img src="img/deploy_3.jpg"  height="350"> |
| |if you plan to host apollo on aws contact<br> info@autosen.com for a cloudformation template. | the io-key only supports TLS encrypted communication.<br>  contact info@autosen.com for a certificate for your apollo. | 


Please note that your apollo container is configured for easy deployment. Therefore some features are disabled by default. E.g.  the Data Connector does not use TLS encryption for MQTT by default.


## Start apollo container
Download the [docker-compose.yml](https://gitlab.com/autosen_gmbh/apollo/-/raw/master/docker-compose.yml?inline=false) or clone this repository.

Configure your MQTT broker (Northbound__MqttBroker) in the *docker-compose.yml*. 
In case you don't have an mqtt broker you can use the docker-compose.yml in the apollo_and_northbound_mosquitto subfolder. It will start a local northbound mosquitto broker together with apollo.

apollo will publish all measurement data to the topic "processdata".

apollo will publish all alarms the topic "alarms".

Then run apollo. Open a command prompt in the apollo directory and run
```
 docker-compose up
```

Note: the logs may show a "database not found" exception on the first apollo start. This is normal behavior. The database is created automatically after the exception was logged. 

## Command Connector-API
General assumptions
- an io-key's unique identifier is its IMEI. 

## Using Swagger

Assuming you run apollo on your computer (e.g. using Docker Desktop) you can use the Swagger UI at:

[http://localhost/hubmanagement/swagger/index.html](http://localhost/hubmanagement/swagger/index.html)

Please also see the [Postman section](#device-provisioning) for more details about the request order, constrains and behavior.

## Getting Started with Postman
Open postman [Download Link](https://www.postman.com/downloads/)
Import the apollo environment and collection


<img src="img/import.jpg"  width="300">

<br>
<img src="img/import2.jpg"  width="650">
<br>

Activate the apollo environment. 
If you are not running apollo on your computer you have to change the environment's "host" variable and enter the IP or URL of your apollo instance.<br>
<img src="img/env_active.jpg"  width="650">

### Device provisioning
Before an io-key can connect to apollo it has to be provisioned. 
Therefore you have to send the "provision request" to apollo. 

After you ran the provisioning request you see the Success Code 204.<br> 
<img src="img/provision2.jpg"  width="650">


to check the result use the 'Get all devices' request

<img src="img/getdevices.jpg"  width="650">

### Run io-key simulator
#### Option 1: Standalone Windows application
Start the io-key simulator by executing 
```
00_SoftwareIoKey.exe
```
If you are not running apollo on the same computer as the apollo container you have to change the apollo address in the *appsettings.json* in the same directory as the *00_SoftwareIoKey.exe*. 

#### Option 2: Docker

see [io-key_simulator docker/Readme.md](io-key_simulator_docker/README.md)

### Measurement Data Format
By default the sensor values are published to the topic *processdata/{imei}* (e.g. *processdata/123456789012345*). See [Northbound MQTT Topics](#northbound-mqtt-topics) how to customize the topic. 

The json payload always contains these attribues


- **source** {imei}-{SensorName}-{slot}
- **value** current sensor value
- **unit** unit of the data point, as described in the sensor's IODD file
- **time** UTC timestamp when the data was received by apollo
- **name** name of the data point, as described in the sensor's IODD file

MQTT Payload example
```JSON
{
  "source":"123456789012345-AL009-1",
  "value":177.5,
  "unit":"mm",
  "time":"2022-04-24T10:41:53.9092683Z",
  "name":"Distance"
}
```

### Read Sensor Details
Once the device is successfully connected and has published the information about the connected sensors to apollo, you can retrieve the information from apollo using 

- Get all devices (hubmanagement/v1/device)
  - to get all connected devices 
  - use "detailed=true" option to receive details about all data points of all connected sensor.
- Get device (hubmanagement/v1/device/{imei})
  - to get the details only for one io-key
- Get sensors (hubmanagement/v1/device/{imei}/sensors)
  - to get the details only for the sensors of one io-key
- Get sensors (hubmanagement/v1/device/{imei}/sensors/{sensorName}/{slot})
  - to get the details only for one sensor of one io-key
  - plese note that sensorName and slot are required since the information of sensors is persistent even if you connect a new sensor to the same slot.

The response body contains infos for each data point
- **name** Name of the data point
  - can be used for [(Un-)Subscribe datasource](#un-subscribe-datasource)
- **subscribed** is the data point subscribed (true/false)
- **unit** unit of the data point 
- **min** minimum value that the sensor can measure, can be null (e.g. for single bit data point)
- **max** maximum value that the sensor can measure, can be null (e.g. for single bit data point)
- **subindex** subindex of the data point (see IO-Link specification for details). Relevant for setting edge alarm parameters.

<img src="img/getdevices_details_running_sensor.jpg"  width="650">


### (Un-)Subscribe datasource
By default all data points (aka data sources) of all sensors are sent to your broker. 

You can unsubscribe individual data points using the "hubmanagement/v1/datasources/{{imei}}/{{sensorName}}/{{slot}}/unsubscribe" endpoint. 

Send a list of datasource names that shall no longer be sent to your broker. 

If you want to re-subscribe a data point use the  "hubmanagement/v1/datasources/{{imei}}/{{sensorName}}/{{slot}}/subscribe" end point. 

<img src="img/unsubscribe.jpg"  width="650">

# Add MQTT TLS Support for io-key connections

Prerequisites:

- Public certificate (Full chain)
  - can be obtained from autosen. Contact info@autosen.com providing the FQDN / DNS entry of your apollo.
- The certificate's private key

1. Generate .pfx formatted certificate

```bash
openssl pkcs12 -export -in fullchain.crt -inkey private_key.key -out cert.pfx
```

Note: Enter password when prompted
Note: "-in" requires the full certificate chain

2. Base64 encode cert

```bash
cat cert.pfx | base64 > cert.base64.txt
```
Note: the pipe '|' does not work correctly on windows. If you have installed any base64 command line tool on windows you should use: 
```bash
base64 -i cert.pfx -o cert.base64.txt 
```


3. Added the following env's to the docker container

* `HubConfiguration__Mqtt__Encryption__Port` inbound port for io-key connections
  *  must be set to *8883*
* `HubConfiguration__Mqtt__Encryption__Certificate` Base64 encoded certificate in pfx format. 
  * the io-key only accepts certificates issued by autosen. Certificates issued by public CAs and self signed certificates are not supported. 
  * apollo certificate can be obtained from autosen, free of charge.
* `HubConfiguration__Mqtt__Encryption__Password` password for the certificate 

For example in the Dockerfile
```Dockerfile
ENV HubConfiguration__Mqtt__Encryption__Port=8883
ENV HubConfiguration__Mqtt__Encryption__Certificate=<BASE 64 ENCODED .PFX>
ENV HubConfiguration__Mqtt__Encryption__Password=<PASSWORD>
```
or 

```YAML
environment:
    ### io-key inbound MQTT connection settings
    - HubConfiguration__Mqtt__Encryption__Port=8883
    - HubConfiguration__Mqtt__Encryption__Certificate=<BASE_66_ENCODED_PFX>
    - HubConfiguration__Mqtt__Encryption__Password=<Password>
```

# Northbound MQTT TLS

Use the following Environment Variables to configure northbound MQTT+TLS 
* `Northbound__MqttBroker__TLS` switch TLS on/off for northbound MQTT
  * *true* use TLS for northbound MQTT connection
  * *false* default, do not use TLS for northbound MQTT connection
* `Northbound__MqttBroker__ValidateTLS` switch TLS validation on/off for northbound MQTT
  * *true* default, only trust valid certificates from a well know CA or certificates that are  signed by the given CA-certificate
  * *false* trust every server certificate, including self signed certificates
* `Northbound__MqttBroker__Certificate` client certificate as base64
  * see [Readme TLS](./Readme.md#add-mqtt-tls-support) for more details about the format
* `Northbound__MqttBroker__Certificate_Password` password for the client certificate
* `Northbound__MqttBroker__CA` CA certificate for server certificate as base64
  * see [Readme TLS](./Readme.md#add-mqtt-tls-support) for more details about the format

# Advanced Configuration

## Northbound MQTT Topics
You can change the topics where apollo publishes the process data and alarms (northbound).
* `Northbound__MqttBroker__ProcessdataPrefix` set / change the topic prefix where processdata is published
  * default *processdata* 
* `Northbound__MqttBroker__AlarmsPrefix` set / change the topic prefix where alarms ar published
  * * default *alarms* 
* `Northbound__MqttBroker__UseProcessdataSensorSuffix`
  * if *true* the full topic where sensor data is published is
    * *{ProcessdataPrefix}/{imei}/Slot-{SlotID}/{Sensorname}*
  * if *false* (default) the full topic where sensor data is published is
    * *{ProcessdataPrefix}/{imei}*
* `Northbound__MqttBroker__UseAlarmsSensorSuffix`
  * if *true* (default) the full topic where alarms are published is
    * *{AlarmsPrefix}/{imei}/Slot-{SlotID}/{Sensorname}*
  * if *false* the full topic where sensor data is published is
    * *{AlarmsPrefix}/{imei}*

# Known Issues

* Missleading error log messages during startup / connection. These messages can be ignored. 
  * "[Error] [hubmanagement] Error while processing preprocessor ..." 
  * Exceptions on the first startup because the database is not found. After the exception the database is setup / initialized. The exception does only occurs once.


# Terms and conditions

For further information please have a look here:

[https://autosen.com/en/terms_and_conditions](https://autosen.com/en/terms_and_conditions)

[https://autosen.com/eula](https://autosen.com/eula)

[https://media.autosen.com/Shopsystem/Assets/general/FOSS_apollo.txt](https://media.autosen.com/Shopsystem/Assets/general/FOSS_apollo.txt)

[https://media.autosen.com/Shopsystem/Assets/general/FOSS_io-key.txt](https://media.autosen.com/Shopsystem/Assets/general/FOSS_io-key.txt) 


