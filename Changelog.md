# apollo Changelog

## 20210809 
* Add TLS support for io-key MQTT connections
  * see [Readme TLS](./Readme.md#add-mqtt-tls-support) for more details about the format

## 20210912 
* Added TLS support for northbound MQTT 
  * see [Northbound MQTT TLS](./Readme.md#northbound-mqtt-tls) for more details 
* Added customization options for northbound mqtt topics
  * see [Northbound MQTT Topics](./Readme.md#northbound-mqtt-topics) for more details 

## 20220308
  * improve logging (less false errors)
  * don't trim leading slash on procesdata topic
  * Date format of processdata timestamp is now a valid UTC timestamp including the trailing 'z'


## Breaking Changes
  * 20210912: the default *alarms* topic changed from *"alarms/{imei}/{Sensorname}/{Slot}"* to *"{AlarmsPrefix}/{imei}/slot-{slot}/{Sensorname}"* to be consistent with the mqtt recomendataions and the measurement data topic
  * 20220308: Date format of processdata timestamp is now a valid UTC timestamp including the trailing 'z'
